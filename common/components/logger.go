package components

import (
	"encoding/json"
	"fmt"
	"time"

	"gitee.com/shijun123456/kcgin"
	"gitee.com/shijun123456/kcgin/logs"
)

//InitLogger InitLogger
func InitLogger(filepath string) (err error) {
	//打印环境变量
	logs.Info("Environment Variable:MSF_ENV:", kcgin.KcConfig.RunMode)
	//输出文件名和行号
	logs.EnableFuncCallDepth(true)
	logs.SetLogFuncCallDepth(3)

	//开启异步缓存
	logs.Async(1e3)

	fileName := time.Now().Format("20060102") + ".log"
	logConf := make(map[string]interface{})
	if filepath == ""{
		filepath = "/var/log/"
	}
	logConf = map[string]interface{}{
		"filename": filepath + kcgin.AppConfig.String("jaeger.serviceName") + "/" + fileName,
		"maxdays":  1,
	}

	confStr, err := json.Marshal(logConf)
	if err != nil {
		fmt.Println("marshal failed,err:", err)
		return
	}
	logs.SetLogger(logs.AdapterFile, string(confStr))
	return
}
