//配置基础数据库引擎
//配置基础数据库参数
package base

import (
	"fmt"
	"gitee.com/shijun123456/kcgin"
	"gitee.com/shijun123456/kcgin/logs"
	"gitee.com/shijun123456/kcgin/orm"
	_ "github.com/go-sql-driver/mysql"
	"strconv"
)

var(
	err error
)

//初始化驱动
func init(){
	logs.Info("Init driver.go mysql start")
	//设置驱动数据库连接参数
	dataSource := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=%s",kcgin.AppConfig.String("db.user"),kcgin.AppConfig.String("db.pwd"),
		kcgin.AppConfig.String("db.host"),kcgin.AppConfig.String("db.port"),kcgin.AppConfig.String("db.name"),kcgin.AppConfig.String("db.charset"))
	//打印连接数据库参数
	logs.Info("DatabaseDriverConnect String:",dataSource)
	maxIdle,_:= strconv.Atoi(kcgin.AppConfig.DefaultString("db.maxidle","10"))
	maxConn,_:= strconv.Atoi(kcgin.AppConfig.DefaultString("db.maxconn","0"))
	maxTime := kcgin.AppConfig.DefaultInt("db.maxlifetime", 10800)
	logs.Info("connMaxLifeTime(s)：", maxTime)
	//设置注册数据库
	if err == nil{
		err = orm.RegisterDataBase("default", kcgin.AppConfig.String("db.type"), dataSource,maxIdle,maxConn,maxTime)
	}
}