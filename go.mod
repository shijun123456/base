module gitee.com/shijun123456/base

go 1.12

require (
	gitee.com/shijun123456/kcgin v1.0.10
	github.com/codahale/hdrhistogram v0.0.0-20161010025455-3a0bb77429bd // indirect
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lestrrat-go/file-rotatelogs v2.3.0+incompatible
	github.com/lestrrat-go/strftime v1.0.3 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/smallnest/rpcx v0.0.0-20200104074109-4f758db059eb
	github.com/uber/jaeger-client-go v2.21.1+incompatible
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
)
