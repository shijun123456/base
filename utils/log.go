package utils

// 输入日志到文件
// @author liyang<654516092@qq.com>
// @date  2020/7/13 17:16
import (
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"io"
	"log"
	"os"
	"strings"
)

var (
	Logerr *log.Logger // 重要的信息
)

type KcLog struct {
}

//实例
func (k *KcLog) GetInstance(filePath string, logLevel ...string) *KcLog {
	logL := "Info"
	if len(logLevel) > 0 {
		logL = logLevel[0]
	}
	file, _ := rotatelogs.New(filePath)
	Logerr = log.New(io.MultiWriter(file, os.Stderr),
		strings.ToUpper(logL)+"：",
		log.Ldate|log.Ltime|log.Lshortfile)

	return k
}

//写日志
func (k *KcLog) PrintLog(logInfo string) {
	Logerr.Println(logInfo)
}
